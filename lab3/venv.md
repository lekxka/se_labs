# Kreiranje virtualnog environmenta

```
python -m venv NAZIV_VENVA
```

# Aktivacija venva

```
source NAZIV_VENVA/Scripts/activate
```

# Deaktivacija venva

```
deactivate
```

Ili zatvoriti prozor gita pa otvoriti novi
