from django.shortcuts import get_object_or_404, render
from app.models import Image, Comment

# Create your views here.
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'

def profile(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    images = Image.objects.filter(user=user).all()
    comments = Comment.objects.filter(user=user).all()

    context = {
		'user': user,
		'images': images,
		'comments': comments,
	}

    return render(request, 'accounts/profile.html', context=context)
