import random

number = random.randint(1, 9)
guesses = int(1)

guess = int(input("Pogodi broj: "))
while(number != guess):
    if number > guess:
        print("Broj je za " + str(number - guess) + " veci")
    else: 
        print("Broj je za " + str(guess - number) + " manji")
    guess = int(input("Pogodi broj: "))
    guesses += 1

print("Bravo, pogodili ste od " + str(guesses) + ". i mozete doma")