## Najbolji Git tutorial

## git config

Postavljanje korisničkog imena

## git status

Provjera statusa lokalnog repozitorija

## git add

Praćenje novih datoteka

## git log

Prikaz ko je napravio izmjene i kako se zovu

## git commit

Spremanje promjena

## git diff

Prikaz izmjena

## git push

Postavljanje lokalnog repozitorija na internet

## git pull 

- Povlačenje promjena s udaljenog repozitorija na naš lokalni 
- Koristi se na način:

```
git pull origin master
```

gdje `origin` predstavlja naziv udaljenog repozitorija, a `master` predstavlja naziv brencha
